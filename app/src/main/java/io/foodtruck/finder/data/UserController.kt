package io.foodtruck.finder.data

import android.app.Activity
import android.content.Context
import com.firebase.ui.auth.AuthUI
import com.google.firebase.database.DatabaseReference
import io.foodtruck.finder.model.Session
import io.foodtruck.finder.model.User
import io.realm.Realm

/**
 * Created by ChazzCoin : December 2019.
 *
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

open class UserController {

    // in-memory cache of the loggedInUser object
    var user: User? = null
    private lateinit var database: DatabaseReference

    val isLoggedIn: Boolean
        get() = user != null

    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        user = null
    }

    open fun createUser() {
        val realm = Realm.getDefaultInstance()
        if (realm.where(User::class.java) == null){
            realm.executeTransaction { itRealm ->
                itRealm.createObject(User::class.java)
            }
        }
    }

    fun updateUser(user: User){
        val realm: Realm? = Realm.getDefaultInstance()
        realm?.let { itRealm ->
            itRealm.beginTransaction()
            itRealm.insertOrUpdate(user)
            itRealm.commitTransaction()
        }
    }

    fun logout(mContext: Context, mActivity : Activity) {
        AuthUI.getInstance()
            .signOut(mContext)
            .addOnCompleteListener {
                // ...
                Session.restartApplication(mActivity)
            }
    }

    //GET CURRENT SESSION
    open fun getUser(uid:String): User? {
        var realm: Realm? = Realm.getDefaultInstance()
        var tempUser:User? = null
        realm?.let {
            tempUser = it.where(User::class.java).equalTo("uid", uid).findFirst()
        }?: kotlin.run {
            //Failed
        }
        return tempUser
    }

    fun deleteUser(mContext: Context){
        AuthUI.getInstance()
            .delete(mContext)
            .addOnCompleteListener {
                // ...
                Session.logOut()
            }
    }


    //ADD LOCATION
//    fun addLocationToUser(location: Location) {
//        val realm: Realm? = Realm.getDefaultInstance()
//        realm?.let { itRealm ->
//            itRealm.beginTransaction()
//            val currentUser = SessionsController.getSession().getUser()
//            currentUser.addLocation(location)
//            itRealm.insertOrUpdate(currentUser)
//            itRealm.commitTransaction()
//        }
//    }

    //REMOVE LOCATION
//    fun removeLocation(location: Location) {
//        val realm: Realm? = Realm.getDefaultInstance()
//        realm?.let { itRealm ->
//            itRealm.beginTransaction()
//            val currentUser = SessionsController.getSession().getUser()
//            currentUser.removeLocation(location)
//            itRealm.insertOrUpdate(currentUser)
//            itRealm.commitTransaction()
//        }
//    }

    //GET LOCATION FOR ID
//    fun getLocationForId(locationID: Long) : Location? {
//        val currentUser = SessionsController.getSession().getUser()
//        val locations = currentUser.locations
//        locations?.iterator()?.forEach { (id,location) ->
//            if (id == locationID){
//                return location
//            }
//
//        }
//        return null
//    }

    //GET LOCATION ID FOR NAME
//    fun getLocationIdForName(locationName: String) : Long? {
//        val currentUser = SessionsController.getSession().getUser()
//        val locations = currentUser.locations
//        locations?.iterator()?.forEach { (id,location) ->
//            if (location.locationName == locationName){
//                return id
//            }
//        }
//        return null
//    }


    //
    /**
        - FireBase
     */
    //



//    fun removeUser(){
//        val realm: Realm? = Realm.getDefaultInstance()
//        realm?.let {
//            it.executeTransaction { executeIt ->
//                executeIt.delete(User::class.java)
//            }
//        }
//    }

//    fun login(username: String, password: String): Result<User> {
//        // handle login
////        val result = dataSource.login(username, password)
//
//        if (result is Result.Success) {
//            setLoggedInUser(result.data)
//        }
//
//        return result
//    }

//    private fun setLoggedInUser(user: User) {
//        this.user = user
//        // If user credentials will be cached in local storage, it is recommended it be encrypted
//        // @see https://developer.android.com/training/articles/keystore
//    }
}
