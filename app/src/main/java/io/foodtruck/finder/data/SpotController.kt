package io.foodtruck.finder.data

import io.foodtruck.finder.AuthController
import io.foodtruck.finder.model.Session
import io.foodtruck.finder.model.Spot
import io.realm.Realm
import io.realm.RealmList
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter


/**
 *
 * Created by ChazzCoin : December 2019.
 *
 */

class SpotController {

//    var FORMAT_TIME = DateTimeFormatter.ofPattern("HH:MM")
//    var FORMAT_DATE = DateTimeFormatter.ofPattern("d MMM yyyy")

    companion object {
        var listOfSpots : RealmList<Spot> = RealmList()
    }

    var realm: Realm? = Realm.getDefaultInstance()

    private var spot: Spot? = null
    private val aispot= 1


    fun convertStringToDate(date:String): LocalDate {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-d"))
    }

    fun convertStringToTime(time:String): LocalTime {
        return LocalTime.parse(time, DateTimeFormatter.ofPattern("hh:mm"))
    }

    fun createNewSpot(spot: Spot){
        realm?.let {
            it.beginTransaction()
            it.insert(spot)
            it.cancelTransaction()
        }
    }

    //UPDATE CURRENT SESSION
    fun updateSpot(spot: Spot) {
        if (Session.isLogged) {
            val realm = Realm.getDefaultInstance()
            realm.executeTransaction { realm ->
                realm.copyToRealmOrUpdate(spot) }
        }
    }

    //UPDATE CURRENT SESSION
    fun removeSpot(spot: Spot) {
        if (Session.isLogged) {
            val realm = Realm.getDefaultInstance()
            realm.executeTransaction { itRealm ->
                itRealm.where(Spot::class.java).equalTo("id", spot.id).findAll().deleteFirstFromRealm()
            }
        }
    }
    //SET SPOT LIST
    fun setListOfSpots() {
        try {
            if (realm == null) {
                realm = Realm.getDefaultInstance()
            }
            realm?.let {
                val spotsResults = it.where(Spot::class.java).equalTo("spotManager", AuthController.USER_UID).findAll()
                listOfSpots.addAll(spotsResults.subList(0, spotsResults.size))
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //GET CURRENT SESSION
    fun getSpotBySpotManager(username: String): Spot? {
        try {
            if (realm == null) {
                realm = Realm.getDefaultInstance()
            }
            realm?.let {
                spot = it.where(Spot::class.java).equalTo("spotManager", username).findFirst()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return spot
    }

}
