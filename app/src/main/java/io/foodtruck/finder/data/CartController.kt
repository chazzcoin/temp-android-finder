package io.foodtruck.finder.data

import android.content.Context
import android.widget.Toast
import io.foodtruck.finder.model.Cart
import io.foodtruck.finder.model.Spot
import io.realm.Realm
import io.realm.RealmList as RealmList

/**
 *
 * Created by ChazzCoin : December 2019.
 *
 */

class CartController {

//    var realm: Realm? = Realm.getDefaultInstance()
    private var cart: Cart? = null
    var allSpots : RealmList<Spot>? = null

    //CREATE NEW SESSION
    fun createCart() {
        val realm = Realm.getDefaultInstance()
        val cart = Cart()
        realm?.let { itRealm ->
            itRealm.beginTransaction()
            itRealm.createObject(Cart::class.java)
            itRealm.insert(cart)
            itRealm.commitTransaction()
        }
    }

    //UPDATE CURRENT SESSION
    fun updateCart(spot: Spot, mContext : Context){
        val realm = Realm.getDefaultInstance()
        if (checkCartForSpot(spot)) {
            realm?.let { itRealm ->
                itRealm.beginTransaction()
                val cart = itRealm.where(Cart::class.java).equalTo("id", "111").findFirst()
                cart?.addSpot(spot)
                itRealm.commitTransaction()
                Toast.makeText(mContext, "Successfully added Spot to Cart!", Toast.LENGTH_LONG).show()
            }
        } else {
            Toast.makeText(mContext, "Sorry, this spot has already been added to your Cart!", Toast.LENGTH_LONG).show()
        }
    }

    private fun checkCartForSpot(spot: Spot) : Boolean {
        val realm = Realm.getDefaultInstance()
        realm?.let { itRealm ->
            val cart = itRealm.where(Cart::class.java).equalTo("id", "111").findFirst()
            cart?.spots.let { itSpots ->
                val spots : RealmList<Spot>? = itSpots
                spots?.let { itSpot ->
                    //cant for loop!!!
                    for (index in itSpot){
                        if (spot.id == index.id) {
                            return false
                        }
                    }
                }
            }
        }
        return true
    }

    //REMOVE CART
    fun removeCart(){
        val realm = Realm.getDefaultInstance()
        realm?.let {
            it.beginTransaction()
            it.delete(Cart::class.java)
            it.commitTransaction()
        }
    }

    //GET CURRENT CART
    fun getCart(): Cart? {
        val realm = Realm.getDefaultInstance()
        realm?.let {
            cart = it.where(Cart::class.java).equalTo("id", "111").findFirst()
        }
        return cart
    }

    //GET ALL SPOTS IN CART
    fun getSpots() : RealmList<Spot>? {
        val realm = Realm.getDefaultInstance()
        realm?.let { itRealm ->
            val cart = itRealm.where(Cart::class.java).equalTo("id", "111").findFirst()
            cart?.spots.let { itSpots ->
                allSpots = itSpots
            }
        }
        return allSpots
    }
}
