package io.foodtruck.finder.data

import android.content.Context
import com.google.firebase.database.*
import io.foodtruck.finder.AuthController
import io.foodtruck.finder.model.Location
import io.foodtruck.finder.model.Session
import io.foodtruck.finder.utils.FireHelper
import io.foodtruck.finder.utils.showFailedToast
import io.foodtruck.finder.utils.showSuccess

/**
 *
 * Created by ChazzCoin : December 2019.
 *
 */

class LocationController {

    private lateinit var database: DatabaseReference

    /**
     *
     * FIREBASE
     *
     */

    fun getLocationsFromFirebase(mContext : Context ) {
        //TODO: REMOVE ALL LOCATIONS AND UPDATE REALM WITH FRESH SET FROM FIREBASE
        database = FirebaseDatabase.getInstance().reference
        database.child(FireHelper.PROFILES).child(FireHelper.LOCATIONS).child(AuthController.USER_UID)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (ds in dataSnapshot.children) {
                        //TODO: PARSE SPOTS
                        val locations: Location? = ds.getValue(Location::class.java)
                        locations?.let {
                            Session.addLocation(it)
                        }
                    }
                }
                override fun onCancelled(databaseError: DatabaseError) {
                    //TODO: ADD CRASHALYTICS HERE
                    showFailedToast(mContext)
                }
            })
    }

    //
    fun saveLocationToFirebase(location : Location, mContext : Context ) {
        database = FirebaseDatabase.getInstance().reference
        database.child(FireHelper.PROFILES).child(FireHelper.LOCATIONS)
            .child(AuthController.USER_UID).child(location.id.toString())
            .setValue(location)
            .addOnSuccessListener {
                //TODO("HANDLE SUCCESS")
                showSuccess(mContext)
            }.addOnCompleteListener {
                //TODO("HANDLE COMPLETE")
            }.addOnFailureListener {
                //TODO("HANDLE FAILURE")
                showFailedToast(mContext)
            }
    }

    //
    fun removeLocationToFirebase(location : Location, mContext : Context ) {
        database = FirebaseDatabase.getInstance().reference
        database.child(FireHelper.PROFILES).child(FireHelper.LOCATIONS)
            .child(AuthController.USER_UID).child(location.id.toString())
            .removeValue()
            .addOnSuccessListener {
                //TODO("HANDLE SUCCESS")
                showSuccess(mContext)
            }.addOnCompleteListener {
                //TODO("HANDLE COMPLETE")
            }.addOnFailureListener {
                //TODO("HANDLE FAILURE")
                showFailedToast(mContext)
            }
    }

}



