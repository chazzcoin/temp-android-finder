package io.foodtruck.finder.model

import androidx.room.PrimaryKey
import io.realm.RealmList
import io.realm.RealmObject

/**
 * Created by ChazzCoin : December 2019.
 */
open class Cart : RealmObject() {

    companion object {
        var TOTAL_COST : Int = 0
    }

    @PrimaryKey
    var id : String = "111"

    var spots: RealmList<Spot>? = null
    var totalCost: Int = 0 // 2323 20th Ave South

    fun addSpot(spot:Spot){
        this.spots?.add(spot)
        calculateTotalCost()
    }

    //TODO: FIX THIS!!!!
    private fun calculateTotalCost(){
        spots?.let { itSpots ->
            this.totalCost = 0
            for (index in itSpots){
                index.price?.let { itPrice ->
                    this.totalCost.plus(itPrice.toInt())
                }
            }
        }
    }

    fun priceToInt(price:String) : Int {
        return price.toInt()
    }


}



