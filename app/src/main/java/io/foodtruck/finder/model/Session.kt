package io.foodtruck.finder.model

import android.app.Activity
import android.content.Intent
import android.util.Log
import androidx.core.app.ActivityCompat
import io.foodtruck.finder.AuthController
import io.foodtruck.finder.model.Cart
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by ChazzCoin : December 2019.
 */
open class Session : RealmObject() {
    //DO NOT MAKE STATIC!
    @PrimaryKey
    var sessionId = 0

    //DO NOT MAKE STATIC
    var user: User? = null

    //    public void addLocation(Location location) {
    //        this.locations.add(location);
    //    }
    var locations: RealmList<Location>? = null
    var foodtrucks: RealmList<FoodTruck>? = null
    fun addFoodtruck(foodtruck: FoodTruck?) {
        foodtrucks?.add(foodtruck)
    }

    // -> STATIC <- \\
    companion object {
        //App Setup
        var isWaiting = false
        var isAdmin = false
        var isLocationManager = false
        var isFoodTruckManager = false

        //USER AUTH
        private const val ADMIN = "admin"
        private const val WAITING = "waiting"
        private const val LOCATION_MANAGER = "location_manager"
        private const val FOODTRUCK_MANAGER = "foodtruck_manager"
        private val userAuth: String?
            get() {
                val user = user
                return user?.auth ?: WAITING
            }
        private val isUserAuthAdmin: Boolean
            get() = userAuth == ADMIN
        private val isUserAuthLocationManager: Boolean
            get() = userAuth == LOCATION_MANAGER
        private val isUserAuthFoodTruckManager: Boolean
            get() = userAuth == FOODTRUCK_MANAGER
        val isUserAuthWaiting: Boolean
            get() = userAuth == WAITING

        fun sessionSetup() {
            if (isLogged) {
                when {
                    isUserAuthAdmin -> { isAdmin = true }
                    isUserAuthLocationManager -> { isLocationManager = true }
                    isUserAuthFoodTruckManager -> { isFoodTruckManager = true }
                }
            } else { isWaiting = true }
        }

        /** -> Controller Methods <- >  */
        private const val aisession = 1
        var USER_UID = ""
        var userVerified = true

        //Class Variables
        private var mRealm = Realm.getDefaultInstance()

        //GET CURRENT SESSION
        var session: Session? = null
            get() {
                try {
                    if (mRealm == null) { mRealm = Realm.getDefaultInstance() }
                    field = mRealm.where(Session::class.java).equalTo("sessionId", aisession).findFirst()
                    if (field == null) {
                        field = Session()
                        field?.sessionId = aisession
                    }
                } catch (e: Exception) { e.printStackTrace() }
                return field
            }
            private set

        //GET CURRENT USER
        var user: User? = null
            get() {
                try {
                    if (mRealm == null) { mRealm = Realm.getDefaultInstance() }
                    field = mRealm.where(User::class.java).findFirst()
                    if (field == null) { field = User() }
                } catch (e: Exception) { e.printStackTrace() }
                return field
            }
            private set


        //CREATE NEW SESSION
        fun createSession(user: User?): Session? {
            if (mRealm == null) { mRealm = Realm.getDefaultInstance() }
            //Guest guest = SessionsController.getSession().getGuest();
            val session = session
            Log.e("loggedUser", "_wait__")
            session?.let { itSession ->
                itSession.user = user
                mRealm.beginTransaction()
                mRealm.copyToRealmOrUpdate(session)
                mRealm.commitTransaction()
            }
            return session
        }

        //CHECK IS USER IS LOGGED IN
        val isLogged: Boolean
            get() {
                if (mRealm == null) { mRealm = Realm.getDefaultInstance() }
                val session = session
                if (session != null && session.isValid) {
                    val user = session.user
                    if (user != null && user.isValid) {
                        return true
                    }
                }
                return false
            }

        //LOG CURRENT USER OUT
        fun logOut() {
            if (mRealm == null) { mRealm = Realm.getDefaultInstance() }
            mRealm.executeTransaction {
                mRealm.where(Session::class.java).findAll().deleteAllFromRealm()
                mRealm.where(User::class.java).findAll().deleteAllFromRealm()
                mRealm.where(Location::class.java).findAll().deleteAllFromRealm()
                mRealm.where(FoodTruck::class.java).findAll().deleteAllFromRealm()
                mRealm.where(Cart::class.java).findAll().deleteAllFromRealm()
                mRealm.where(Spot::class.java).findAll().deleteAllFromRealm()
            }
        }


        //SYSTEM RESTART THE APP
        fun restartApplication(context: Activity) {
            logOut()
            ActivityCompat.finishAffinity(context)
            context.startActivity(Intent(context, AuthController::class.java))
        }

        //-> LOCATIONS <-\\
        //ADD LOCATION
        fun addLocation(location: Location?) {
            if (mRealm == null) { mRealm = Realm.getDefaultInstance() }
            val session = session
            session?.let { itSession ->
                mRealm.beginTransaction()
                itSession.locations?.add(location)
                mRealm.copyToRealmOrUpdate(session)
                mRealm.commitTransaction()
            }
        }

        //REMOVE LOCATION
        fun removeLocation(location: Location?) {
            if (mRealm == null) { mRealm = Realm.getDefaultInstance() }
            val session = session
            session?.let { itSession ->
                mRealm.beginTransaction()
                itSession.locations?.remove(location)
                mRealm.copyToRealmOrUpdate(session)
                mRealm.commitTransaction()
            }
        }


        //-> FOODTRUCKS <-\\
    }
}