package io.foodtruck.finder.model

import androidx.room.PrimaryKey
import io.realm.RealmObject

/**
 * Created by ChazzCoin : December 2019.
 */
open class FoodTruck(id:Long? = 0,
                     userId:String? = "", truckName:String? = "",
                     truckType:String? = "") : RealmObject() {

    @PrimaryKey
    var id: Long? = 0 //System.getcurrentmilli
    var userId : String? = "" //ID given to user from firebase
    var truckName: String? = "" //Name Given by Manager
    var truckType: String? = ""

    init {
        this.id = id
        this.userId = userId
        this.truckName = truckName
        this.truckType = truckType
    }

}



