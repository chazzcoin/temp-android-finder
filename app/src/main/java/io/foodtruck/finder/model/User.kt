package io.foodtruck.finder.model

import androidx.room.PrimaryKey
import io.realm.RealmObject

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
/**
 * Created by ChazzCoin : December 2019.
 */
open class User(uid:String = "", name:String? = "", email:String? = "") : RealmObject() {

    companion object {
        var ADMIN = "admin"
        var LOCATION_MANAGER = "location_manager"
        var FOODTRUCK_MANAGER = "foodtruck_manager"
        var WAITING = "waiting"
    }

    @PrimaryKey
    var uid = ""

    var name: String? = null
    var email: String? = null
    var auth: String? = null
    var phone: String? = null
//    var locations: Location = null

    init {
        this.uid = uid
        this.name = name
        this.email = email
        this.auth = ""
    }

//    fun addLocation(location: Location){
//        location.id?.let { this.locations?.put(it, location) }
//    }
//
//    fun removeLocation(location: Location){
//        this.locations?.remove(location.id)
//    }

}

