package io.foodtruck.finder.model

import androidx.room.PrimaryKey
import io.realm.RealmObject

/**
 * Created by ChazzCoin : December 2019.
 */
open class Spot(id:String? = "", addressOne:String? = "", addressTwo:String? = "",
                city:String? = "", state:String? = "", zip:String? = "",
                 date:String? = "", spotManager:String? = "") : RealmObject() {

    companion object {
        const val BREAKFAST = 111
        const val LUNCH = 222
        const val DINNER = 333

        const val LUNCH_TIME : String = "11AM-2PM"
        const val DINNER_TIME : String = "5PM-8PM"

        const val PRICE : String = "5.00"

        const val AVAILABLE : String = "available"
        const val PENDING : String = "pending"
        const val BOOKED : String = "booked"

        const val ENTREE : String = "Entree"
        const val DESSERT : String = "Dessert"
    }
    // DateTimeFormatter.ofPattern("d MMM yyyy")
    // DateTimeFormatter.ofPattern("hh:mm")

    @PrimaryKey
    var id: String? = ""

    var addressOne: String? = "" // 2323 20th Ave South
    var addressTwo: String? = "" // 2323 20th Ave South
    var city: String? = "" // Birmingham
    var state: String? = "" // AL
    var zip: String? = "" // 35223

    var date: String? = "" // 10 Dec 2019
    var foodType: String? = "" //Entree, Dessert. . .
    var mealType: String? = "" //Breakfast, Lunch or Dinner?
    var estPeople: String? = "" //Amount of expected people
    var status : String? = AVAILABLE //Has it been bought?
    var price: String? = "" //Assigned Price to Spot
    var spotManager: String? = "" //Creators Display Name
    var assignedTruckUid : String? = "" //FoodTruck who buys Spot
    var assignedTruckName : String? = "" //FoodTruck who buys Spot
    var locationName: String? = "" //Custom Name Made by Creator
    var locationUUID: String? = "" //Custom Name Made by Creator

    init {
        this.id = id
        this.addressOne = addressOne
        this.addressTwo = addressTwo
        this.city = city
        this.state = state
        this.zip = zip
        this.date = date
        this.spotManager = spotManager
        this.price = if (price.isNullOrBlank() || price.isNullOrEmpty()) {PRICE} else {price}
    }

}

fun Spot.convertToMap() : MutableMap<String,String> {
    val spot = this
    return mutableMapOf<String,String>().apply {
        this["id"] = spot.id as String
        this["locationName"] = spot.locationName as String
        this["locationUUID"] = spot.locationUUID as String
        this["addressOne"] = spot.addressOne as String
        this["addressTwo"] = spot.addressTwo as String
        this["city"] = spot.city as String
        this["state"] = spot.state as String
        this["zip"] = spot.zip as String
//        this["parkingInfo"] = spot.parkingInfo
        this["estPeople"] = spot.estPeople as String
//        this["mealTime"] = spot.mealTime
        this["foodType"] = spot.foodType as String
        this["date"] = spot.date as String
        this["spotManager"] = spot.spotManager as String
        this["price"] = spot.price as String
        this["status"] = spot.status as String
        this["assignedTruckUid"] = spot.assignedTruckUid as String
        this["assignedTruckName"] = spot.assignedTruckName as String
    }
}

class ParseSpot {

    fun parseSpot(map:HashMap<String,String>) : Spot {
        val newSpot = Spot()
        newSpot.id = map["id"]
        newSpot.locationName = map["locationName"]
        newSpot.locationUUID = map["locationUUID"]
        newSpot.addressOne = map["addressOne"]
        newSpot.addressTwo = map["addressTwo"]
        newSpot.city = map["city"]
        newSpot.state = map["state"]
        newSpot.zip = map["zip"]
//        newSpot.parkingInfo = map["parkingInfo"]
        newSpot.date = map["date"]
        newSpot.spotManager = map["spotManager"]
//        newSpot.parseMealTime(str: (map["mealTime"])
        newSpot.foodType = map["foodType"]
        newSpot.estPeople = map["estPeople"]
        newSpot.assignedTruckUid = map["assignedTruckUid"]
        newSpot.assignedTruckName = map["assignedTruckName"]
        newSpot.status = map["status"]
        newSpot.price = map["price"]
        return newSpot
    }

}

