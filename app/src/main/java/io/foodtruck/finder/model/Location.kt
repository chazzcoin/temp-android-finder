package io.foodtruck.finder.model

import androidx.room.PrimaryKey
import io.realm.RealmObject

/**
 * Created by ChazzCoin : December 2019.
 */
open class Location(id:String? = "", locationName:String? = "", addressOne:String? = "", addressTwo:String? = "",
                    city:String? = "", state:String? = "", zip:String? = "",
                    estPeople:String? = "") : RealmObject() {

    @PrimaryKey
    var id: String? = "" //UUID

//    var userId : String? = "" //ID given to user from firebase
    var locationName: String? = "" //Name Given by Manager
    var addressOne: String? = "" // 2323 20th Ave South
    var addressTwo: String? = "" // 2323 20th Ave South
    var city: String? = "" // Birmingham
    var state: String? = "" // AL
    var zip: String? = "" // 35223
    var typeOfPlace: String? = ""
    var estPeople: String? = ""

    init {
        this.id = id
//        this.userId = userId
        this.locationName = locationName
        this.addressOne = addressOne
        this.addressTwo = addressTwo
        this.city = city
        this.state = state
        this.zip = zip
        this.estPeople = estPeople
    }

}



