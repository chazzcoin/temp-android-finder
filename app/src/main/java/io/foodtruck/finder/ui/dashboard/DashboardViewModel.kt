package io.foodtruck.finder.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.foodtruck.finder.model.Session
import io.foodtruck.finder.model.User

/**
 * Created by ChazzCoin : December 2019.
 */

class DashboardViewModel : ViewModel() {

    var user: User? = null

    init {
        user = Session.user
    }

    private val _text = MutableLiveData<String>().apply {
        user?.let {
            value = "Welcome, " + it.name
        }
    }

    val text: LiveData<String> = _text
}