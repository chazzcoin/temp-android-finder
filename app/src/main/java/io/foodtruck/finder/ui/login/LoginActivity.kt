package io.foodtruck.finder.ui.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import io.foodtruck.finder.AuthController
import io.foodtruck.finder.locationManager.MainLocationManagerActivity
import io.foodtruck.finder.data.UserController
import io.foodtruck.finder.model.Session
import io.foodtruck.finder.model.User
import io.foodtruck.finder.utils.FireHelper

/**
 * Created by ChazzCoin : December 2019.
 */

class LoginActivity : AppCompatActivity() {

    private var COUNT = 0
    private val RC_SIGN_IN: Int = 999

    private lateinit var database: DatabaseReference
    private lateinit var mUser : User

    // Choose authentication providers
    val providers = arrayListOf(
        AuthUI.IdpConfig.EmailBuilder().build(),
        AuthUI.IdpConfig.GoogleBuilder().build())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        database = FirebaseDatabase.getInstance().reference

        if (Session.isLogged){
            //TODO: MAKE SURE WE ARE SENDING THE USER TO THE RIGHT LOCATION!!
            val user : User? = Session.user
            Toast.makeText(this, "Welcome: " + user?.name, Toast.LENGTH_LONG).show()
            startActivity(Intent(this@LoginActivity, MainLocationManagerActivity::class.java))
        } else {
            startActivityForResult(
                AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .build(),
                RC_SIGN_IN)
        }
    }
    //Ask for auth first -> if null add waiting
    private fun getUserAuthFromFirebase(currentUser : FirebaseUser) {
        database = FirebaseDatabase.getInstance().reference
        database.child(FireHelper.PROFILES).child(FireHelper.USERS).child(currentUser.uid)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    //TODO: PARSE SPOTS
                    val user: User? = dataSnapshot.getValue(User::class.java)
                    mUser = User(currentUser.uid,currentUser.displayName,currentUser.email)
                    user?.let { itUser ->
                        mUser.auth = itUser.auth
                        saveProfileToFirebase()
                    }?: kotlin.run {
                        //No User Move On
                        mUser.auth = User.WAITING
                        saveProfileToFirebase()
                    }
                }
                override fun onCancelled(p0: DatabaseError) {
//                    TODO("not implemented")

                }
            })
    }

    //SAVE PROFILE
    private fun saveProfileToFirebase() {
//        mUser = User(currentUser.uid,currentUser.displayName,currentUser.email)
        database.child(FireHelper.PROFILES).child(FireHelper.USERS).child(mUser.uid).setValue(mUser)
            .addOnSuccessListener {
//                TODO("HANDLE SUCCESS")
                UserController().updateUser(mUser)
                Session.createSession(mUser)
                startActivity(Intent(this@LoginActivity, AuthController::class.java))
            }.addOnCompleteListener {
//                TODO("HANDLE COMPLETE")

            }.addOnFailureListener {
//                TODO("HANDLE FAILURE")
                showLoginFailed()
            }
    }

    private fun showLoginFailed() {
        Toast.makeText(applicationContext, "Error Signing In", Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)
            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                //TODO: SETUP USER MODEL
                val user = FirebaseAuth.getInstance().currentUser
                user?.let {
                    //Grab Auth from Firebase if available
                    getUserAuthFromFirebase(user)
                }
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }
    override fun onRestart() {
        super.onRestart()

//        SessionsController.getSession().getUser()
//        if (SessionsController.isLogged()){
//
//        } else{
//            SessionsController.restartApplication(this)
//        }
//        SessionsController.restartApplication(this)
    }

}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
