package io.foodtruck.finder.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.foodtruck.finder.AuthController
import io.foodtruck.finder.R
import io.foodtruck.finder.model.Session
import io.foodtruck.finder.model.User
import kotlinx.android.synthetic.main.fragment_dashboard.view.*

/**
 * Created by ChazzCoin : December 2019.
 */

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
//        val textView: TextView = root.findViewById(R.id.welcome)

        dashboardViewModel.text.observe(requireActivity(), Observer {
//            textView.text = it
        })

        val user = Session.user

        //FoodTruck Manager
        if (AuthController.USER_AUTH == User.FOODTRUCK_MANAGER){
            root.txtUserWelcome.text = user?.name
            root.txtUserEmail.text = user?.email
            //Location Manager
        } else if (AuthController.USER_AUTH == User.LOCATION_MANAGER){
            root.txtUserEmail.text = "Location Manager"
            root.txtUserWelcome.text = user?.name
            root.txtUserEmail.text = user?.email
        }

        //
        root.btnLogout.setOnClickListener {
            if (Session.isLogged){
                Session.logOut()
                Session.restartApplication(requireActivity())
            }
        }
        return root
    }

    fun setupLocation() {


    }
}