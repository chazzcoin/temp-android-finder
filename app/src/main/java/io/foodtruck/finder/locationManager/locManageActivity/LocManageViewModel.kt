package io.foodtruck.finder.locationManager.locManageActivity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * Created by ChazzCoin : December 2019.
 */

class LocManageViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Manage Locations"
    }
    val text: LiveData<String> = _text
}