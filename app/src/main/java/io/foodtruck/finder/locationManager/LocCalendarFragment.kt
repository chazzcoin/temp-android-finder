package io.foodtruck.finder.locationManager

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import io.foodtruck.finder.R
import io.foodtruck.finder.data.SpotController
import io.foodtruck.finder.model.Location
import io.foodtruck.finder.model.Session
import io.foodtruck.finder.model.Spot
import io.foodtruck.finder.model.User
import io.foodtruck.finder.utils.*
import io.realm.RealmList
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.calendar_day_legend.view.*
import kotlinx.android.synthetic.main.spot_calendar_day.view.*
import kotlinx.android.synthetic.main.spot_calendar_fragment.*
import kotlinx.android.synthetic.main.spot_calendar_item_view.view.*
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth
import org.threeten.bp.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * Created by ChazzCoin : December 2019.
 *
 * --!!FOR LOCATION MANAGERS!!--
 */

private val Context.inputMethodManager
    get() = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

class SpotCalendarAdapterLocation(val onClick: (Spot) -> Unit) :
    RecyclerView.Adapter<SpotCalendarAdapterLocation.SpotsViewHolderLocation>() {

    val spots = mutableListOf<Spot>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpotsViewHolderLocation {
        return SpotsViewHolderLocation(parent.inflate(R.layout.spot_calendar_item_view))
    }

    override fun onBindViewHolder(viewHolder: SpotsViewHolderLocation, position: Int) {
        viewHolder.bind(spots[position])
    }

    override fun getItemCount(): Int = spots.size

    inner class SpotsViewHolderLocation(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        init {
            itemView.setOnClickListener {
                onClick(spots[adapterPosition])
            }
        }

        fun bind(spot: Spot) {
            //TODO: SETUP DESIGN HERE
            //Get the Spot Manager's name for UID
            containerView.itemSpotText.text = spot.spotManager
        }
    }
}

//------------------------------------------------------------------------------------------------//
/**
 * FRAGMENT BEGINS HERE
 */
//------------------------------------------------------------------------------------------------//

class SpotCalendarFragmentLocation : Fragment(), AdapterView.OnItemSelectedListener  {

    private val spotsAdapterLocation =
        SpotCalendarAdapterLocation {
            //TODO: ONLY SHOW THEIR SPOTS? NO ONE ELSES?
            AlertDialog.Builder(requireContext())
                .setMessage(R.string.spot_dialog_delete_confirmation)
                .setPositiveButton(R.string.delete) { index, pos ->

                    //TODO: DELETE SPOT
                    removeSpotFromFirebase(it, pos)
                }
                .setNegativeButton(R.string.close, null)
                .show()
        }

//    private var mapOfLocations : HashMap<String?, Location?> = HashMap()

    private lateinit var database: DatabaseReference

    private var selectedDate: LocalDate? = null
    private val today = LocalDate.now()
    private lateinit var mContext : Context

    //Make a Spot
    private lateinit var mThis : SpotCalendarFragmentLocation
    private lateinit var rView : View
    private lateinit var eSpinSpotLocation : Spinner
    private lateinit var eCheckLunch : CheckBox
    private lateinit var eCheckDinner : CheckBox
    private var locationMap : HashMap<Int, Location> = HashMap()
    private var locationList : RealmList<Location> = RealmList() // -> ORIGINAL LIST
    private var finalLocation : Location? = null
    private var locationNameList : ArrayList<String?> = ArrayList() // -> USED FOR INPUT DIALOG

    private lateinit var eSpinAdapter : ArrayAdapter<String?>
    private var isLunch = false
    private var isDinner = false

    private val titleSameYearFormatter = DateTimeFormatter.ofPattern("MMMM")
    private val titleFormatter = DateTimeFormatter.ofPattern("MMM yyyy")
    private val selectionFormatter = DateTimeFormatter.ofPattern("d MMM yyyy")
    private val spots = mutableMapOf<LocalDate, List<Spot>>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        getSpotsFromFirebase()
        return inflater.inflate(R.layout.spot_calendar_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mContext = requireContext()
        mThis = this

        //TODO: GET LOCATIONS AND SET THEM
        Session.session?.locations.let {
            if (it != null) {
                locationList = it
                prepareListOfLocations()
                eSpinAdapter = ArrayAdapter(mContext, android.R.layout.simple_list_item_1, locationNameList)
            }
        }

        //Recycler View
        spotCalendarRecyclerView.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        spotCalendarRecyclerView.adapter = spotsAdapterLocation
        spotCalendarRecyclerView.addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL))

        val daysOfWeek = daysOfWeekFromLocale()
        val currentMonth = YearMonth.now()

        navigation_spot_calendar.setup(currentMonth.minusMonths(10), currentMonth.plusMonths(10), daysOfWeek.first())
        navigation_spot_calendar.scrollToMonth(currentMonth)

        if (savedInstanceState == null) {
            navigation_spot_calendar.post {
                // Show today's events initially.
                selectDate(today)
            }
        }

        //Day Selector
        class DayViewContainer(view: View) : ViewContainer(view) {
            lateinit var day: CalendarDay // Will be set when this container is bound.
            val textView = view.spotCalendarDayText
            val dotView = view.spotCalendarDotView

            init {
                view.setOnClickListener {
                    if (day.owner == DayOwner.THIS_MONTH) {
                        selectDate(day.date)
                    }
                }
            }
        }

        //Day Binder
        navigation_spot_calendar.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                val textView = container.textView
                val dotView = container.dotView

                textView.text = day.date.dayOfMonth.toString()

                if (day.owner == DayOwner.THIS_MONTH) {
                    textView.makeVisible()
                    when (day.date) {
                        today -> {
                            textView.setTextColorRes(R.color.example_3_white)
                            textView.setBackgroundResource(R.drawable.spot_today_bg)
                            dotView.makeInVisible()
                        }
                        selectedDate -> {
                            textView.setTextColorRes(R.color.example_3_blue)
                            textView.setBackgroundResource(R.drawable.spot_selected_bg)
                            dotView.makeInVisible()
                        }
                        else -> {
                            textView.setTextColorRes(R.color.example_3_black)
                            textView.background = null
                            dotView.isVisible = spots[day.date].orEmpty().isNotEmpty()
                        }
                    }
                } else {
                    textView.makeInVisible()
                    dotView.makeInVisible()
                }
            }
        }

        //Scroll Listener
        navigation_spot_calendar.monthScrollListener = {
            (activity as AppCompatActivity).supportActionBar?.title = if (it.year == today.year) {
                titleSameYearFormatter.format(it.yearMonth)
            } else {
                titleFormatter.format(it.yearMonth)
            }
            // Select the first day of the month when
            // we scroll to a new month.
            selectDate(it.yearMonth.atDay(1))
        }

        //Month Container
        class MonthViewContainer(view: View) : ViewContainer(view) {
            val legendLayout = view.legendLayout
        }

        //Month Binder
        navigation_spot_calendar.monthHeaderBinder = object : MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                // Setup each header day text if we have not done that already.
                if (container.legendLayout.tag == null) {
                    container.legendLayout.tag = month.yearMonth
                    container.legendLayout.children.map { it as TextView }.forEachIndexed { index, tv ->
                        tv.text = daysOfWeek[index].name.first().toString()
                        tv.setTextColorRes(R.color.example_3_black)
                    }
                }
            }
        }
        //TODO: ADD SPOT FAB BUTTON
        spotAddButton.setOnClickListener {
            // Create and Show AlertDialog
            inputDialog().create().show()
        }
    }

    private fun getFreshSpots() {

        SpotController().setListOfSpots()
        spotsAdapterLocation.spots.clear()
        spotsAdapterLocation.spots.addAll(SpotController.listOfSpots)
        spotsAdapterLocation.notifyDataSetChanged()

    }

    private fun prepareListOfLocations() {
        var i = 0
        for (location in locationList){
            locationNameList.add(location.locationName)
            locationMap[i] = location
            i++
        }
    }

    private fun getSpotTime() : String {
        return if (isLunch){
            "lunch"
        } else {
            "dinner"
        }
    }

    //TODO: ADD SPOT FAB BUTTON
    private fun inputDialog() : AlertDialog.Builder {
        Log.d("SpotCalendarLocation: ", "inputDialog")
        val builder = AlertDialog.Builder(requireContext())
        builder.apply {

            rView = View.inflate(requireContext(), R.layout.dialog_spot_creation,null)
            eSpinSpotLocation = rView.findViewById(R.id.spinSpotLocation)
            eSpinSpotLocation.onItemSelectedListener = mThis
            eSpinSpotLocation.adapter = eSpinAdapter

            eCheckLunch = rView.findViewById(R.id.checkBoxLunch)
            eCheckDinner = rView.findViewById(R.id.checkBoxDinner)

            //LUNCH
            eCheckLunch.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    isLunch = true
                    isDinner = false
                    eCheckDinner.isEnabled = false
                } else {
                    isLunch = false
                    isDinner = false
                    eCheckDinner.isEnabled = true
                }
            }
            //DINNER
            eCheckDinner.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    isDinner = true
                    isLunch = false
                    eCheckLunch.isEnabled = false
                } else {
                    isDinner = false
                    isLunch = false
                    eCheckLunch.isEnabled = true
                }
            }

            val username: User? = Session.user
            this.setView(rView)
            //Buttons
            setPositiveButton("Save"){ _,_ ->
                finalLocation?.let { ifFinalLocation ->
                    //Create Spot
                    val spot = Spot(
                        UUID.randomUUID().toString(), //uuid here!
//                        System.currentTimeMillis(),
                        ifFinalLocation.addressOne, ifFinalLocation.addressTwo,
                        ifFinalLocation.city, ifFinalLocation.state, ifFinalLocation.zip,
                        selectedDate.toString(), getSpotTime(),
//                        username?.uid
                    )
                    //Save Spot
                    saveSpotAtSelectedDate(spot)
                    saveSpotToFirebase(spot)
                }

            }
            setNegativeButton("Cancel") { dialog,_ ->
                dialog.dismiss()
            }
        }
        return builder
    }

    private fun selectDate(date: LocalDate) {
        if (selectedDate != date) {
            val oldDate = selectedDate
            selectedDate = date
            oldDate?.let { navigation_spot_calendar.notifyDateChanged(it) }
            navigation_spot_calendar.notifyDateChanged(date)
            updateAdapterForDate(date)
        }
    }

    private fun saveSpotAtDateTime(spot: Spot) {
        spot.date?.let {
            val temp = SpotController().convertStringToDate(it)
            spots[temp] = spots[temp].orEmpty().plus(spot)
            updateAdapterForDate(temp)
        }
    }

    private fun saveSpotAtSelectedDate(spot: Spot) {
        //LOG
        selectedDate?.let {
            spots[it] = spots[it].orEmpty().plus(spot)
            updateAdapterForDate(it)
        }
    }

//    private fun deleteEvent(event: Spot) {
//        val date = event.date
//        spots[date] = spots[date].orEmpty().minus(event)
//        updateAdapterForDate(date)
//    }

    private fun updateAdapterForDate(date: LocalDate) {
        spotsAdapterLocation.spots.clear()
        spotsAdapterLocation.spots.addAll(spots[date].orEmpty())
        spotsAdapterLocation.notifyDataSetChanged()
        spotCalendarSelectedDateText.text = selectionFormatter.format(date)
    }

    private fun updateAdapter(date: LocalDate) {
//        spotsAdapterLocation.spots.clear()
//        spotsAdapterLocation.spots.remove(spot)
        spotsAdapterLocation.notifyDataSetChanged()
        spotCalendarSelectedDateText.text = selectionFormatter.format(date)
    }

    //SAVE SPOT
    private fun saveSpotToFirebase(spot : Spot) {
        Log.d("SpotCalendarLocation: ", "saveSpotToFirebase")
        database = FirebaseDatabase.getInstance().reference
        database
            .child(FireHelper.AREAS).child(FireHelper.ALABAMA).child(FireHelper.BIRMINGHAM)
            .child(FireHelper.SPOTS).child(spot.id.toString()).setValue(spot)
            .addOnSuccessListener {
                //TODO("HANDLE SUCCESS")
                SpotController().createNewSpot(spot)
                showSuccess(requireContext())
            }.addOnCompleteListener {
                //TODO("HANDLE COMPLETE")
            }.addOnFailureListener {
                //TODO("HANDLE FAILURE")
                showFailedToast(requireContext())
            }
    }

    //REMOVE SPOT
    private fun removeSpotFromFirebase(spot : Spot, pos: Int) {
        Log.d("SpotCalendarLocation: ", "removeSpotToFirebase")
        database = FirebaseDatabase.getInstance().reference
        database
            .child(FireHelper.AREAS).child(FireHelper.ALABAMA).child(FireHelper.BIRMINGHAM)
            .child(FireHelper.SPOTS).child(spot.id.toString()).removeValue()
            .addOnSuccessListener {
                //TODO("HANDLE SUCCESS")
                SpotController().removeSpot(spot)
                getFreshSpots()
                this.selectedDate?.let {
                    updateAdapter(it)
                }
//                spotsAdapterLocation.notifyItemRemoved(pos)
                showSuccess(requireContext())
            }.addOnCompleteListener {
                //TODO("HANDLE COMPLETE")
            }.addOnFailureListener {
                //TODO("HANDLE FAILURE")
                showFailedToast(requireContext())
            }
    }

    private fun getSpotsFromFirebase() {
        Log.d("SpotCalendarLocation: ", "getSpotsFromFirebase")
        database = FirebaseDatabase.getInstance().reference
        database.child(FireHelper.AREAS).child(FireHelper.ALABAMA)
            .child(FireHelper.BIRMINGHAM).child(FireHelper.SPOTS)
            .addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (ds in dataSnapshot.children) {
                    //TODO: PARSE SPOTS
                    val spot: Spot? = ds.getValue(Spot::class.java)
                    spot?.let {
//                        saveSpotAtDateTime(it)
                    }
                }
                //DO SETUP FOR CALENDAR
                updateAdapterForDate(LocalDate.now())
                navigation_spot_calendar.refreshDrawableState()
                navigation_spot_calendar.notifyCalendarChanged()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                //TODO: ADD CRASHALYTICS HERE
                showFailedToast(requireContext())
            }
        })
    }

    override fun onStart() {
        super.onStart()
//        (activity as AppCompatActivity).toolbar.setBackgroundColor(requireContext().getColorCompat(R.color.example_3_toolbar_color))
        requireActivity().window.statusBarColor = requireContext().getColorCompat(R.color.example_3_statusbar_color)
    }

    override fun onStop() {
        super.onStop()
//        (activity as AppCompatActivity).toolbar.setBackgroundColor(requireContext().getColorCompat(R.color.colorPrimary))
        requireActivity().window.statusBarColor = requireContext().getColorCompat(R.color.colorPrimaryDark)
    }



    override fun onNothingSelected(parent: AdapterView<*>?) {
        //

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        //match the name of this location with locations in users locations
        //TODO: Get Location for position in adapter that was selected
        finalLocation = locationMap[position]
        Log.d("Location From Spinner: ", finalLocation.toString())
    }
}
