package io.foodtruck.finder.locationManager.locManageActivity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.database.DatabaseReference
import io.foodtruck.finder.R
import io.foodtruck.finder.data.*
import io.foodtruck.finder.model.Location
import io.foodtruck.finder.model.Session
import kotlinx.android.synthetic.main.fragment_locations.view.*
import java.util.*

/**
 * Created by ChazzCoin : December 2019.
 */

class LocManageFragment : Fragment() {

    private lateinit var database: DatabaseReference
    private lateinit var notificationsViewModel: LocManageViewModel
    private lateinit var root : View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        notificationsViewModel = ViewModelProviders.of(this).get(LocManageViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_locations, container, false)
        //Recycler View
//        this.root.recyclerViewLocationsList.layoutManager = LinearLayoutManager(requireContext())
        root.recyclerViewLocationsList.layoutManager = GridLayoutManager(requireContext(), 2)
        this.root.recyclerViewLocationsList.adapter = LocManageViewAdapter(requireContext())

//        setupDisplay()
        root.btnAddLocation.setOnClickListener{
            getLocationInfoFromUser()
//            setupDisplay()
            root.recyclerViewLocationsList.adapter = LocManageViewAdapter(requireContext())
        }

        return root
    }
//    private fun setupDisplay(){
//        val locations = SessionsController.getSession().getUser().locations
//        root.txtNumberLocations.text = locations?.size.toString()
//    }

    private fun getLocationInfoFromUser(){
        //
        val id = UUID.randomUUID().toString()
        val locationName = root.editLocationName.text.toString()
//        val addressOne = root.editAddressOne.toString()
//        val addressTwo = root.editAddressTwo.toString()
//        val city = root.editCity.toString()
//        val state = root.editState.toString()
//        val zip = root.editZip.toString()
//        val estPeople = root.editEstPeople.toString()

//        val locationName = "Test Name"
        val addressOne = "Test Address One $id"
        val addressTwo = "Test Address Two $id"
        val city = "Test City $id"
        val state = "Test State $id"
        val zip = "Test Zip $id"
        val estPeople = "Test estPeople $id"

        val newLocation = Location(id,locationName,addressOne,addressTwo,city,state,zip,estPeople)
        Session.addLocation(newLocation)
        LocationController().saveLocationToFirebase(newLocation, requireContext())
        clearAllFields()

    }


    private fun clearAllFields(){
        root.editLocationName.text.clear()
        root.editAddressOne.text.clear()
        root.editAddressTwo.text.clear()
        root.editCity.text.clear()
        root.editState.text.clear()
        root.editZip.text.clear()
        root.editEstPeople.text.clear()
    }
}