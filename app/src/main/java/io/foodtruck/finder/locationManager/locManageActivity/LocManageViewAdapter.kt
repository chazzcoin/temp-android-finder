package io.foodtruck.finder.locationManager.locManageActivity

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import io.foodtruck.finder.R
import io.foodtruck.finder.data.LocationController
import io.foodtruck.finder.model.Location
import io.foodtruck.finder.model.Session
import io.foodtruck.finder.utils.inflate
import io.realm.RealmList
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_list_locations.view.*

class LocManageViewAdapter(mContext: Context) : RecyclerView.Adapter<LocManageViewAdapter.InnerLocationViewHolder>() {

    var locationList : RealmList<Location>? = Session.session?.locations
    var arrayOfLocations : ArrayList<Location> = ArrayList()
    var context = mContext

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InnerLocationViewHolder {
        reloadLocations()
        return InnerLocationViewHolder(parent.inflate(R.layout.item_list_locations))
    }

    override fun onBindViewHolder(viewHolder: InnerLocationViewHolder, position: Int) {

        arrayOfLocations.let {
            it[position].let { it1 ->
                viewHolder.bind(it1)
            }
        }

        viewHolder.itemView.setOnLongClickListener{
            arrayOfLocations[position].let { it1 ->
                AlertDialog.Builder(context)
                    .setMessage(R.string.location_dialog_delete_confirmation)
                    .setPositiveButton(R.string.delete) { _, _ ->
                        //TODO: DELETE SPOT
//                        UserController().removeLocationFromUser(it1)
                        Session.removeLocation(it1)
                        LocationController().removeLocationToFirebase(it1, mContext = context)
                        this.reloadLocations()
                        this.notifyDataSetChanged()
                    }
                    .setNegativeButton(R.string.close, null)
                    .show()
            }
            return@setOnLongClickListener true
        }
    }

    override fun getItemCount(): Int {
        if (arrayOfLocations.isEmpty()){
            reloadLocations()
        }
        arrayOfLocations.let {
            return it.size
        }
    }

    private fun reloadLocations(){
        this.locationList = Session.session?.locations
        arrayOfLocations.clear()
        locationList?.iterator()?.forEach { itLocation ->
            arrayOfLocations.add(itLocation)
        }
    }


    inner class InnerLocationViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        init {
            itemView.setOnClickListener {
//                onClick(spots[adapterPosition])
            }


        }

        fun bind(locations: Location) {
            //TODO: SETUP DESIGN HERE
            containerView.itemLocationId.text = locations.id.toString()
            containerView.itemLocationName.text = locations.locationName
        }
    }
}