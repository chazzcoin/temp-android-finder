package io.foodtruck.finder.foodtruckManager

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import io.foodtruck.finder.R
import io.foodtruck.finder.data.CartController
import io.foodtruck.finder.data.SpotController
import io.foodtruck.finder.model.Spot
import io.foodtruck.finder.utils.*
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.calendar_day_legend.view.*
import kotlinx.android.synthetic.main.spot_calendar_day.view.*
import kotlinx.android.synthetic.main.spot_calendar_fragment.*
import kotlinx.android.synthetic.main.spot_calendar_item_view.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth
import org.threeten.bp.format.DateTimeFormatter

/**
 * Created by ChazzCoin : December 2019.
 *
 * --!!FOR FOOD TRUCK MANAGERS!!--
 */

class SpotCalendarFragmentFoodTruck : Fragment() {

    private val spotsAdapterDelete2 =
        SpotCalendarAdapterFoodTruck {
            AlertDialog.Builder(requireContext())
                .setMessage(R.string.spot_dialog_delete_confirmation)
                .setPositiveButton(R.string.delete) { _, _ ->
                    //TODO: DELETE SPOT
//                deleteEvent(it)
                }
                .setNegativeButton(R.string.close, null)
                .show()
        }

    private val spotsAdapterDelete =
        SpotCalendarAdapterFoodTruck {
            AlertDialog.Builder(requireContext())
                .setMessage(R.string.spot_dialog_add_to_cart)
                .setPositiveButton("Add") { _, _ ->
                    //TODO: Add SPOT to Cart
                    CartController().updateCart(it, requireContext())
                }
                .setNegativeButton(R.string.fui_cancel, null)
                .show()
        }

    private lateinit var database: DatabaseReference

    private var selectedDate: LocalDate? = null
    private val today = LocalDate.now()

    private val titleSameYearFormatter = DateTimeFormatter.ofPattern("MMMM")
    private val titleFormatter = DateTimeFormatter.ofPattern("MMM yyyy")
    private val selectionFormatter = DateTimeFormatter.ofPattern("d MMM yyyy")
    private val spots = mutableMapOf<LocalDate, List<Spot>>()

    //coroutines
    val mainJob = SupervisorJob()
    val ioJob = SupervisorJob()
    val main = CoroutineScope(Dispatchers.Main + mainJob)
    val io = CoroutineScope(Dispatchers.IO + ioJob)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        getSpotsFromFirebase()
        return inflater.inflate(R.layout.spot_calendar_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Recycler View
        spotCalendarRecyclerView.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        spotCalendarRecyclerView.adapter = spotsAdapterDelete
        spotCalendarRecyclerView.addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL))

        val daysOfWeek = daysOfWeekFromLocale()
        val currentMonth = YearMonth.now()

        navigation_spot_calendar.setup(currentMonth.minusMonths(10), currentMonth.plusMonths(10), daysOfWeek.first())
        navigation_spot_calendar.scrollToMonth(currentMonth)

        if (savedInstanceState == null) {
            navigation_spot_calendar.post {
                // Show today's events initially.
                selectDate(today)
            }
        }

        //Day Selector
        class DayViewContainer(view: View) : ViewContainer(view) {
            lateinit var day: CalendarDay // Will be set when this container is bound.
            val textView = view.spotCalendarDayText
            val dotView = view.spotCalendarDotView

            init {
                view.setOnClickListener {
                    if (day.owner == DayOwner.THIS_MONTH) {
                        selectDate(day.date)
                    }
                }
            }
        }

        //Day Binder
        navigation_spot_calendar.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                val textView = container.textView
                val dotView = container.dotView

                textView.text = day.date.dayOfMonth.toString()

                if (day.owner == DayOwner.THIS_MONTH) {
                    textView.makeVisible()
                    when (day.date) {
                        today -> {
                            textView.setTextColorRes(R.color.example_3_white)
                            textView.setBackgroundResource(R.drawable.spot_today_bg)
                            dotView.makeInVisible()
                        }
                        selectedDate -> {
                            textView.setTextColorRes(R.color.example_3_black)
                            textView.setBackgroundResource(R.drawable.spot_selected_bg)
                            dotView.makeInVisible()
                        }
                        else -> {
                            textView.setTextColorRes(R.color.example_3_black)
                            textView.background = null
                            dotView.isVisible = spots[day.date].orEmpty().isNotEmpty()
                        }
                    }
                } else {
                    textView.makeInVisible()
                    dotView.makeInVisible()
                }
            }
        }

        //Scroll Listener
        navigation_spot_calendar.monthScrollListener = {
            (activity as AppCompatActivity).supportActionBar?.title = if (it.year == today.year) {
                titleSameYearFormatter.format(it.yearMonth)
            } else {
                titleFormatter.format(it.yearMonth)
            }
            // Select the first day of the month when
            // we scroll to a new month.
            selectDate(it.yearMonth.atDay(1))
        }

        //Month Container
        class MonthViewContainer(view: View) : ViewContainer(view) {
            val legendLayout = view.legendLayout
        }

        //Month Binder
        navigation_spot_calendar.monthHeaderBinder = object : MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                // Setup each header day text if we have not done that already.
                if (container.legendLayout.tag == null) {
                    container.legendLayout.tag = month.yearMonth
                    container.legendLayout.children.map { it as TextView }.forEachIndexed { index, tv ->
                        tv.text = daysOfWeek[index].name.first().toString()
                        tv.setTextColorRes(R.color.example_3_black)
                    }
                }
            }
        }
        //TODO: ADD SPOT FAB BUTTON
        spotAddButton.hide()
//        spotAddButton.setOnClickListener {
//            // Create and Show AlertDialog
//            inputDialog().create().show()
//        }
    }

    //TODO: ADD SPOT FAB BUTTON
    private fun inputDialog() : AlertDialog.Builder {
        val builder = AlertDialog.Builder(requireContext())
        builder.apply {
            val rView = View.inflate(requireContext(), R.layout.dialog_spot_add_to_cart,null)
//            val eAddressOne = rView.findViewById<EditText>(R.id.editAddressOne)
            this.setView(rView)
            //Buttons
            setPositiveButton("Add"){ _,_ ->
                Toast.makeText(requireContext(), "Added to Cart!", Toast.LENGTH_LONG).show()
            }
            setNegativeButton("Cancel") { dialog,_ ->
                dialog.dismiss()
            }
        }
        return builder
    }

    private fun selectDate(date: LocalDate) {
        if (selectedDate != date) {
            val oldDate = selectedDate
            selectedDate = date
            oldDate?.let { navigation_spot_calendar.notifyDateChanged(it) }
            navigation_spot_calendar.notifyDateChanged(date)
            updateAdapterForDate(date)
        }
    }

    private fun saveSpotAtDateTime(spot: Spot) {
        spot.date?.let {
            val temp = SpotController().convertStringToDate(it)
            spots[temp] = spots[temp].orEmpty().plus(spot)
            updateAdapterForDate(temp)
        }
    }

    private fun saveSpotAtSelectedDate(spot: Spot) {
        selectedDate?.let {
            spots[it] = spots[it].orEmpty().plus(spot)
            updateAdapterForDate(it)
        }
    }

//    private fun deleteEvent(event: Spot) {
//        val date = event.date
//        spots[date] = spots[date].orEmpty().minus(event)
//        updateAdapterForDate(date)
//    }

    private fun updateAdapterForDate(date: LocalDate) {
        spotsAdapterDelete.spots.clear()
        spotsAdapterDelete.spots.addAll(spots[date].orEmpty())
        spotsAdapterDelete.notifyDataSetChanged()
        spotCalendarSelectedDateText.text = selectionFormatter.format(date)
    }

    //SAVE PROFILE
    private fun saveSpotToFirebase(spot : Spot) {
        database = FirebaseDatabase.getInstance().reference
        database
            .child(FireHelper.AREAS).child(FireHelper.ALABAMA).child(FireHelper.BIRMINGHAM)
            .child(FireHelper.SPOTS).child(spot.id.toString()).setValue(spot)
            .addOnSuccessListener {
                //TODO("HANDLE SUCCESS")
                SpotController().createNewSpot(spot)
                showSuccess(requireContext())
            }.addOnCompleteListener {
                //TODO("HANDLE COMPLETE")
            }.addOnFailureListener {
                //TODO("HANDLE FAILURE")
                showFailedToast(requireContext())
            }
    }

    private fun getSpotsFromFirebase() {

        //TODO: fire up some coroutines here to handle the waiting for return obj.
//        val dataSnapshot = FireHelper().getSpotsFromFirebaseByMonth("Jul2020") ?: return
//        for (ds in dataSnapshot.children) {
//            //TODO: PARSE SPOTS
//            val spot: Spot? = ds.getValue(Spot::class.java)
//            spot?.let {
//                saveSpotAtDateTime(it)
//            }
//        }
        //DO SETUP FOR CALENDAR
        updateAdapterForDate(LocalDate.now())
        navigation_spot_calendar.refreshDrawableState()
        navigation_spot_calendar.notifyCalendarChanged()

    }

//    private fun getSpotsFromFirebase() {
//        database = FirebaseDatabase.getInstance().reference
//        database.child(FireHelper.AREAS).child(FireHelper.ALABAMA)
//            .child(FireHelper.BIRMINGHAM).child(FireHelper.SPOTS)
//            .addListenerForSingleValueEvent(object : ValueEventListener {
//            override fun onDataChange(dataSnapshot: DataSnapshot) {
//                for (ds in dataSnapshot.children) {
//                    //TODO: PARSE SPOTS
//                    val spot: Spot? = ds.getValue(Spot::class.java)
//                    spot?.let {
//                        saveSpotAtDateTime(it)
//                    }
//                }
//                //DO SETUP FOR CALENDAR
//                updateAdapterForDate(LocalDate.now())
//                navigation_spot_calendar.refreshDrawableState()
//                navigation_spot_calendar.notifyCalendarChanged()
//            }
//
//            override fun onCancelled(databaseError: DatabaseError) {
//                //TODO: ADD CRASHALYTICS HERE
//                showFailed(requireContext())
//            }
//        })
//    }

    override fun onStart() {
        super.onStart()
//        (activity as AppCompatActivity).toolbar.setBackgroundColor(requireContext().getColorCompat(R.color.example_3_toolbar_color))
        requireActivity().window.statusBarColor = requireContext().getColorCompat(R.color.example_3_statusbar_color)
    }

    override fun onStop() {
        super.onStop()
//        (activity as AppCompatActivity).toolbar.setBackgroundColor(requireContext().getColorCompat(R.color.colorPrimary))
        requireActivity().window.statusBarColor = requireContext().getColorCompat(R.color.colorPrimaryDark)
    }
}

private val Context.inputMethodManager
    get() = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager


/** SPOT CALENDAR ADAPTER FOODTRUCK **/

class SpotCalendarAdapterFoodTruck(val onClick: (Spot) -> Unit) :
    RecyclerView.Adapter<SpotCalendarAdapterFoodTruck.SpotsViewHolderFoodTruck>() {

    val spots = mutableListOf<Spot>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpotsViewHolderFoodTruck {
        return SpotsViewHolderFoodTruck(parent.inflate(R.layout.spot_calendar_item_view))
    }

    override fun onBindViewHolder(viewHolder: SpotsViewHolderFoodTruck, position: Int) {
        viewHolder.bind(spots[position])
    }

    override fun getItemCount(): Int = spots.size

    inner class SpotsViewHolderFoodTruck(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        init {
            itemView.setOnClickListener {
                onClick(spots[adapterPosition])
            }
        }

        fun bind(spot: Spot) {
            //TODO: SETUP DESIGN HERE
            containerView.itemSpotText.text = spot.spotManager
        }
    }
}
