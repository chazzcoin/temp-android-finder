package io.foodtruck.finder.foodtruckManager.cart

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.foodtruck.finder.R
import io.foodtruck.finder.data.CartController
import io.foodtruck.finder.model.Spot
import io.foodtruck.finder.utils.inflate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_list_spot.view.*

class CartViewAdapter : RecyclerView.Adapter<CartViewAdapter.InnerCartViewHolder>() {

    val cart = CartController().getCart()
    val spots = CartController().getSpots()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InnerCartViewHolder {
        return InnerCartViewHolder(parent.inflate(R.layout.item_list_spot))
    }

    override fun onBindViewHolder(viewHolder: InnerCartViewHolder, position: Int) {
        spots?.let {
            it[position]?.let { it1 -> viewHolder.bind(it1) }
        }
    }

    override fun getItemCount(): Int {
        spots?.let {
            return it.size
        } ?: kotlin.run {
            return 0
        }
    }

    inner class InnerCartViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        init {
            itemView.setOnClickListener {
//                onClick(spots[adapterPosition])
            }
        }

        fun bind(spot: Spot) {
            //TODO: SETUP DESIGN HERE
            containerView.itemSpotManager.text = spot.spotManager
            containerView.itemSpotPrice.text = "$" + spot.price + ".00"
        }
    }
}