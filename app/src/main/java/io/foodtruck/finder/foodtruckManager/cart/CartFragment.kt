package io.foodtruck.finder.foodtruckManager.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.foodtruck.finder.R
import io.foodtruck.finder.data.CartController
import kotlinx.android.synthetic.main.fragment_cart.view.*

/**
 * Created by ChazzCoin : December 2019.
 */

class CartFragment : Fragment() {

    private lateinit var cartViewModel: CartViewModel
    private lateinit var root: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        cartViewModel = ViewModelProviders.of(this).get(CartViewModel::class.java)
        root = inflater.inflate(R.layout.fragment_cart, container, false)

        //Recycler View
        root.recyclerViewCart.layoutManager = LinearLayoutManager(requireContext())
        root.recyclerViewCart.adapter = CartViewAdapter()

        setupDisplay()
        root.btnClearCart.setOnClickListener{
            CartController().removeCart()
            CartController().createCart()
            setupDisplay()
            root.recyclerViewCart.adapter = CartViewAdapter()
        }
        return root
    }

    private fun setupDisplay(){
        //Cart SETUP
        val cart = CartController().getCart()
        cart?.let { itCart ->
            root.cartSpots.text = "Spots in Cart: " + itCart.spots?.size.toString()
            root.txtCartTotal.text = "$" + itCart.totalCost.toString() + ".00"
        }
    }
}