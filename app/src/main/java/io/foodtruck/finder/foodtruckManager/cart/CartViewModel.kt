package io.foodtruck.finder.foodtruckManager.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * Created by ChazzCoin : December 2019.
 */

class CartViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Purchase Spots in your Cart"
    }
    val text: LiveData<String> = _text
}