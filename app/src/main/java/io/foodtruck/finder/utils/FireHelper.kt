package io.foodtruck.finder.utils

import android.content.Context
import com.google.firebase.database.*
import io.foodtruck.finder.model.Session
import io.foodtruck.finder.model.Spot
import kotlinx.coroutines.*

/**
 * Created by ChazzCoin : December 2019.
 */

class FireHelper {

    companion object {
        const val spotMonthDB = "MMMyyyy"
        const val DATE_MONTH = "MMMM"

        const val AVAILABLE: String = "available"
        const val PENDING: String = "pending"
        const val BOOKED: String = "booked"
        const val WAITING: String = "waiting"

        const val paymentIntentUrl = "https://us-central1-food-truck-finder-91dc0.cloudfunctions.net/charge/"
        const val FIRE_DATE_FORMAT = "EEE, MMM d yyyy, hh:mm:ss a"
        //ADMIN
        const val ADMIN: String = "admin"
        const val FOODTRUCK_MANAGER: String = "foodtruck_manager"
        const val LOCATION_MANAGER: String = "location_manager"
        const val PROFILES: String = "Profiles"
        const val USERS: String = "users"
        const val LOCATIONS: String = "locations"
        const val FOODTRUCKS: String = "foodtrucks"
        //City
        const val AREAS: String = "areas"
        const val ALABAMA: String = "alabama"
        const val BIRMINGHAM: String = "birmingham"
//        const val MONTH: String? = null
        const val SPOTS: String = "spots"
    }
    var database: FirebaseDatabase = FirebaseDatabase.getInstance()

    //coroutines
    var mainJob = SupervisorJob()
    var ioJob = SupervisorJob()
    var main = CoroutineScope(Dispatchers.Main + mainJob)
    var io = CoroutineScope(Dispatchers.IO + ioJob)


    val profiles: DatabaseReference
        get() = database.getReference(PROFILES)

    val admin: DatabaseReference
        get() = database.getReference(ADMIN)

    val users: DatabaseReference
        get() {
            val myRef: DatabaseReference = database.getReference(PROFILES)
            return myRef.child(USERS)
        }

    fun getSpotsFromFirebaseByMonth2(MONTH:String, context: Context? = null) {

        io.launch {  }

    }


    // Foodtrucks get their Spots by Month
    private suspend fun getSpotsFromFirebaseByMonth(MONTH:String, context: Context?)
            = withContext(io.coroutineContext) {
        var dbObj : DataSnapshot? = null
        val database = FirebaseDatabase.getInstance().reference
        database.child(AREAS).child(ALABAMA)
            .child(BIRMINGHAM).child(SPOTS).child(MONTH)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    //todo: parse and save
                    for (ds in dataSnapshot.children) {
                        //TODO: PARSE SPOTS
                        val spot: Spot? = ds.getValue(Spot::class.java)
                        spot?.let {

                        }
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    //TODO: ADD CRASHALYTICS HERE
                    context?.let { itContext ->
                        showFailedToast(itContext)
                    }
                }
            })
    }
}