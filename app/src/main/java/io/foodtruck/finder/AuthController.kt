package io.foodtruck.finder

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.FirebaseApp
import com.google.firebase.database.*
import com.jakewharton.threetenabp.AndroidThreeTen
import io.foodtruck.finder.data.CartController
import io.foodtruck.finder.data.UserController
import io.foodtruck.finder.model.User
import io.foodtruck.finder.foodtruckManager.MainFoodTruckManagerActivity
import io.foodtruck.finder.locationManager.MainLocationManagerActivity
import io.foodtruck.finder.model.Session
import io.foodtruck.finder.ui.login.LoginActivity
import io.foodtruck.finder.utils.FireHelper
import io.foodtruck.finder.utils.showFailedToast
import io.realm.Realm
import io.realm.RealmConfiguration

/**
 * Created by ChazzCoin : December 2019.
 */

class AuthController : AppCompatActivity()  {

    companion object {
        var USER_UID = ""
        var USER_AUTH = ""
    }

    private val mInstance: AuthController? = null
    private lateinit var database: DatabaseReference

    var mUser : User? = null
    lateinit var uid : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //TODO: SETUP ALL INIT'S

        //Init Firebase Instance
        FirebaseApp.initializeApp(this)
        //Init Calendar
        AndroidThreeTen.init(this)

        //Init Realm DB
        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
            .name(BuildConfig.APPLICATION_ID + ".realm")
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(realmConfiguration)

        if (Session.isLogged){
            mUser = Session.user
            mUser?.let { itUser ->
                uid = itUser.uid
                USER_UID = itUser.uid
                USER_AUTH = itUser.auth.toString()
                //Init Cart
                CartController().createCart()
                getProfileUpdatesFirebase()
            }
        } else {
            //Create User Object (checks if null)
            UserController().createUser()
            Toast.makeText(this, "No User", Toast.LENGTH_LONG).show()
            startActivity(Intent(this@AuthController, LoginActivity::class.java))
        }
    }

    @Synchronized
    fun getInstance(): AuthController? {
        return mInstance
    }

    override fun onRestart() {
        super.onRestart()
        if (Session.isLogged){
            val user : User? = Session.user
            Toast.makeText(this, "Welcome: " + user?.name, Toast.LENGTH_LONG).show()
            startActivity(Intent(this@AuthController, MainLocationManagerActivity::class.java))
        } else {
            Toast.makeText(this, "No User", Toast.LENGTH_LONG).show()
            startActivity(Intent(this@AuthController, LoginActivity::class.java))
        }
    }

    private fun getProfileUpdatesFirebase() {
        database = FirebaseDatabase.getInstance().reference
        database.child(FireHelper.PROFILES).child(FireHelper.USERS).child(uid)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val temp: User? = dataSnapshot.getValue(User::class.java)
                    temp?.let {
                        if (it.uid == uid){
                            USER_AUTH = it.auth.toString()
                            UserController().updateUser(it)
                            navigateUser(it)
                        }
                    }
                }
                override fun onCancelled(databaseError: DatabaseError) {
                    showFailedToast(this@AuthController)
                }
            })
    }

    private fun navigateUser(user: User){
        when (user.auth) {
            User.FOODTRUCK_MANAGER -> {
                Toast.makeText(this, "Foodtruck Manager", Toast.LENGTH_LONG).show()
                startActivity(Intent(this@AuthController, MainFoodTruckManagerActivity::class.java))
            }
            User.LOCATION_MANAGER -> {
                Toast.makeText(this, "Location Manager", Toast.LENGTH_LONG).show()
                startActivity(Intent(this@AuthController, MainLocationManagerActivity::class.java))
            }
            else -> {
                Toast.makeText(this, "Pending User Approval", Toast.LENGTH_LONG).show()
                //TODO: CREATE TEMP PAGE FOR WAITING USERS
                startActivity(Intent(this@AuthController, MainPendingActivity::class.java))
            }
        }
    }

}
